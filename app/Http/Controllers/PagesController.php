<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;
use App\Post;

class PagesController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy( 'created_at', 'desc' )->limit( 4 )->get();
    	return view( 'pages.index' )->withPosts( $posts );
    }

    public function getContact()
    {
    	return view( 'pages.contact' );
    }

    public function postContact( Request $request )
    {
        $this->validate( $request, [ 
            'email' => 'required|email',
            'subject' => 'required|min:3',
            'message' => 'required|min:10'
        ] );

        $data = [ 
            'email' => $request->email, 
            'subject' => $request->subject, 
            'bodyMessage' => $request->message
        ];

        Mail::send( 'emails.contact', $data, function( $message ) use ( $data ){
            $message->from( $data[ 'email'] );
            $message->to( 'mwineafrod@gmail.com' );
            $message->subject( $data[ 'subject'] );

        } );
    }

    public function about()
    {
        $firstname = 'Mwine';
        $lastname = 'Melik';
        $age = 23;
        $full = $firstname . ' ' . $lastname;
        $data = [];
        $data[ 'full' ] = $full;
        $data[ 'age' ] = $age;

        //return view( 'pages.about' )->withData( $data );
        return view( 'pages.about', compact( 'data', $data ) );
        //return view( 'pages.about' )->with( 'fullname' => $full );
        //return view( 'pages.about' )->withFullname( $full )->withAge( $age );
        //return view( 'pages.about' )->with( [ 'fullname' => $full, 'age' => $age ] );

    	//return view( 'pages.about',compact( [ 'firstname' => 'firstname', 'lastname' => 'lastname' ] ) );
    }
}
