@extends( 'main' )
@section( 'title', '| Contact' )
@section( 'content' )
        <div class="row">
            <div class="col-md-12">
                  <h1>Contact Me!</h1>
                  <hr>
                  <form action="/postMail" method="POST">
                    {{ csrf_field() }}
                      <div class="form-group">
                          <label name="email">Email:</label>
                          <input id="email" name="email" class="form-control">
                      </div>

                      <div class="form-group">
                          <label name="subject">subject:</label>
                          <input id="subject" name="subject" class="form-control">
                      </div>

                      <div class="form-group">
                          <label name="message">message:</label>
                          <textarea id="message" name="message" class="form-control">Type your message here...</textarea>
                      </div>

                      <input type="submit" value="send Messgae" class="btn btn-success">
                  </form>
            </div>
        </div>
@endsection